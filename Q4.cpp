#include <stdio.h>
int main() {
    int number, i;
    printf("Enter a number: ");
    scanf("%i",&number);
    printf("Factors of %i are: ", number);
    for(i=1; i <= number ; i++) {
        if (number%i == 0) printf("\n %i ",i);
    }
    printf("\n");
    return 0;
}
