#include <stdio.h>
int main() {
    int number, reverse;
    printf("Enter a number: ");
    scanf("%i", &number);
    while (number != 0) {
        reverse = (reverse * 10) + (number % 10); //reverse = (reverse*10) + remainder  // remainder = number*10
        number /= 10;
    }
    printf("Reversed number = %i", reverse);
    return 0;
}
